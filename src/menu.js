import NameForm from './js/form.js';
import React, {Component} from "react"

class Intro extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    this.props.myFunc(this);
  }

  render() {
    return <div>
            <h1>INTRO</h1>
            <p>Here is a long description</p>
            <p>Two paragraphs</p>
          </div>;
  }
}

class Redux extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    this.props.myFunc(this);
  }

  render() { 
    const formList = ['name' , 'password', 'email'].map( lab =>
        <NameForm key={lab} label={lab.toUpperCase()} />
    );
    return <div><h1>REDUX</h1>{formList}</div>;
  }
}

export { Intro, Redux };

import React from "react"
import axios from 'axios'

class FormLine extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isHidden: false,
      value: ''
    };

    this.label = props.label || 'Label:';

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    axios.get( 'https://captionflow.com/infra/getUsers')
    .then( resp => {
      console.log( resp );
    });
    //alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          <span>{this.label}</span>
          <input type="text" 
              value={this.state.value} 
              onChange={this.handleChange}
          />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.isHidden = Math.random() > 0.3;
    this.label = props.label || 'Label:';
  }

  render() {
    return (
      <FormLine label={this.label} />
    );
  }
}

export default NameForm;

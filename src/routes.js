import React from "react"
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Link, NavLink, Switch } from "react-router-dom";
//import { BrowserRouter as Router, Route, Link, NavLink, Switch } from "react-router";

import Menu, {Intro, Redux} from './menu.js'

let Component = React.Component;

const Extra = () => <h1>Extra</h1>
const Home  = () => <h1>Home</h1>

const Routes = () => (
  <Router>
    <div>
      <ul className="rightPane">
        <li><NavLink activeClassName="active" to="/home">Home</NavLink></li>
        <li><NavLink activeClassName="active" to="/intro">Intro</NavLink></li>
        <li><NavLink activeClassName="active" to="/redux">Redux</NavLink></li>
        <li><NavLink activeClassName="active" to="/extra">Extra</NavLink></li>
      </ul>

      <div className="leftPane">
        <Switch>
          <Route path="/home"  component={Home} />
          <Route path="/intro" component={Intro} />
          <Route path="/redux" component={Redux} />
          <Route path="/extra" component={Extra} />
          <Route               component={Home} />
        </Switch>
      </div>
    </div>
  </Router>
);

export default Routes;

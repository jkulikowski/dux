import React from "react";
import ReactDOM from "react-dom";
import style from "./css/style.less";

import Routes from './routes.js';

const Header = () => <div className="header">Header</div>
const Footer = () => <div className="footer">Footer</div>

ReactDOM.render(<Header  />, document.getElementById("header"));
ReactDOM.render(<Routes  />, document.getElementById("content"));
ReactDOM.render(<Footer  />, document.getElementById("footer"));

let express = require('express');
let app = express();

app.use(express.static('dist'));

app.use((req, res) => {
  res.sendFile(`${__dirname}/dist/index.html`);
});

app.listen( 7887 );
